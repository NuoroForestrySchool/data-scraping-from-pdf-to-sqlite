This repo's DOI: 10.5281/zenodo.10449354

# Data scraping from PDF to SQLite

An attempt to recover metadata usability  
An exercise in scraping data from a structured PDF  
and organize it a tables in an SQLite file  

See DOI 10.5281/zenodo.10449306 for further explanations
